'use strict';

var React = require('react-native');
var {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Navigator
} = React;
var GameScreen         = require("./src/game_screen");
var AnswerScreen       = require("./src/answer_screen");
var HomeScreen         = require("./src/home_screen");
var InstructionsScreen = require("./src/instructions_screen");

var BaseConfig = Navigator.SceneConfigs.FadeAndroid;
var CustomSceneConfig = Object.assign({}, BaseConfig, {})

var bitflip = React.createClass({
  _renderScene: function(route, navigator) {
    if (route.id === 1) {
      return <HomeScreen navigator={navigator}/>;
    } else if (route.id === 2) {
      return <InstructionsScreen navigator={navigator}/>;
    } else if (route.id === 3) {
      return <GameScreen navigator={navigator}/>;
    } else if (route.id === 4) {
      return <AnswerScreen navigator={navigator} answer={route.answer}/>;
    }
  },
  _configureScene: function() {
    return CustomSceneConfig;
  },
  render: function() {
    return (
      <Navigator
        initialRoute={{
          id: 1
        }}
        renderScene={this._renderScene}
        configureScene={this._configureScene}
      />
    )
  }
});

AppRegistry.registerComponent('bitflip', () => bitflip);
