'use strict'

jest.dontMock('../generator');

describe("Generator", function() {
  it("#numbers should return an array", function() {
    var Generator = require("../generator");
    var g = new Generator();
    expect(g.numbers()).toEqual([])
  });
});
