'use strict';

var React = require('react-native');
var NumberGenerator = require("./generator");
var GridView = require('react-native-grid-view');
var Helper = require("./helper");

var {
  View,
  StyleSheet,
  Text,
  TouchableHighlight,
} = React;

var GameScreen = React.createClass({
  getInitialState: function() {
    return {
      generator: null,
      numbers: null
    }
  },
  componentWillMount: function() {
    var generator = new NumberGenerator(25);
    this.setState({
      generator: generator,
      numbers: generator.numbers()
    });
  },
  _renderItem: function(item) {
    return (
      <View key={Helper.guidGenerator()} style={styles.cell}>
        <Text style={styles.cellText}>
          {item}
        </Text>
      </View>
    );
  },
  _handleYesPress: function() {
    this.state.generator.yes();
    this._handleChoice("yes");
  },
  _handleNoPress: function() {
    this.state.generator.no();
    this._handleChoice("no");
  },
  _handleChoice(choice) {
    if (this.state.generator.numberFound()) {
      this.props.navigator.push({
        id: 4,
        answer: this.state.generator.finalNumber
      });
    } else {
      this.setState({
        numbers: this.state.generator.numbers()
      })
    }

  },
  render: function() {
    return (
      <View style={styles.container}>
        <View style={styles.topSection}>
          <Text style={styles.instructions}>
            Can you see the number below?
          </Text>
        </View>

        <View style={styles.tableSection}>
          <GridView
            scrollEnabled={false}
            items={this.state.numbers}
            itemsPerRow={5}
            renderItem={this._renderItem}
          />
        </View>

        <View style={[styles.gameContainerButtons, styles.bottomSection]}>
          <TouchableHighlight
            style={styles.gameButton}
            onPress={this._handleYesPress}>
            <Text style={styles.buttonTextStyle}>
              Yes
            </Text>
          </TouchableHighlight>

          <TouchableHighlight
            style={styles.gameButton}
            onPress={this._handleNoPress}>
            <Text style={styles.buttonTextStyle}>
              No
            </Text>
          </TouchableHighlight>
        </View>
      </View>
    )
  }
})

var styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flex: 5,
    backgroundColor: '#F5FCFF',
  },
  topSection: {
    paddingTop: 20,
    justifyContent: 'center',
    flex: 1
  },
  tableSection: {
    flex: 3,
  },
  bottomSection: {
    flex: 1,
  },
  cell: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#EFF1F3',
    borderStyle: 'solid',
    borderColor: "#DBD3D8",
    borderWidth: 0.5,
    width: 67,
    height: 67,
  },
  instructions: {
    color: "#223843",
    fontSize: 20,
  },
  cellText: {
    fontSize: 20,
    color: "#D77A61"
  },
  gameContainerButtons: {
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  gameButton: {
    borderColor: "#EFF1F3",
    backgroundColor: "#223843",
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 60,
    width: 169,
  },
  buttonTextStyle: {
    fontSize: 25,
    color: "#EFF1F3",
  }
});

module.exports = GameScreen;
