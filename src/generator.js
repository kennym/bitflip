'use strict'

class Generator {
  constructor(maxNumber) {
    this.randomBit          = this._generateRandomBit();
    this.stepNumber         = 0;
    this.finalNumber        = 0;
    this.maxNumber          = maxNumber;
    this._nearestPowerOfTwo = this._getNearestPowerOfTwo(maxNumber);
  }

  _getNearestPowerOfTwo(number) {
    return Math.pow(2, Math.floor(Math.log2(number)) + 1);
  }

  _generateRandomBit() {
    return Math.floor(Math.random() * 2);
  }

  numberFound() {
    if ((1 << (this.stepNumber + 1)) > this._nearestPowerOfTwo) {
      return true;
    } else {
      return false;
    }
  }

  yes() {
    if (this.numberFound()) {
      return false;
    }
    if (this.randomBit === 1) {
      this.finalNumber += (1 << this.stepNumber);
    }
    this.stepNumber++;
  }

  no() {
    if (this.numberFound()) {
      return false;
    }
    if (this.randomBit === 0) {
      this.finalNumber += (1 << this.stepNumber);
    }
    this.stepNumber++;
  }

  numbers() {
    var numbers = [];
    for (var i = 1; i <= this.maxNumber; i++) {
      if (((i & 1 << this.stepNumber) <= 0) || (this.randomBit == 0)) {
        if (((i & 1 << this.stepNumber) != 0) ||(this.randomBit == 1)) {
          numbers.push(null);
          continue;
        }
      }
      numbers.push(i);
    }
    return numbers;
  }
}

module.exports = Generator;
