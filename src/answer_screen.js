'use strict';

var React = require('react-native');
var {
  View,
  StyleSheet,
  Text,
  TouchableHighlight,
} = React;

var AnswerScreen = React.createClass({
  _handleResetButton: function() {
    this.props.navigator.resetTo({id: 2});
  },
  render: function() {
    return (
      <View style={styles.container}>
        <View style={styles.answerSection}>
          <Text style={styles.headerText}>
            The number is...
          </Text>

          <Text style={styles.finalNumber}>
            {this.props.answer}
          </Text>
        </View>

        <View style={styles.gameButtons}>
          <TouchableHighlight style={styles.resetButton}>
            <Text
              onPress={this._handleResetButton}
              style={styles.resetButtonText}>
              Play again
            </Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  },
});

var styles = StyleSheet.create({
  container: {
    flex: 3,
    backgroundColor: '#F5FCFF',
  },
  answerSection: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    flex: 2,
  },
  headerText: {
    color: "#223843",
    marginBottom: 20,
    fontSize: 18,
  },
  finalNumber: {
    color: "#223843",
    fontSize: 100,
  },
  gameButtons: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
    margin: 20
  },
  resetButton: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#223843",
    borderStyle: 'solid',
    borderColor: "#EFF1F3",
    borderWidth: 1,
    flex: 1,
    height: 60,
  },
  resetButtonText: {
    color: "#EFF1F3",
    fontSize: 25
  }
});

module.exports = AnswerScreen;
