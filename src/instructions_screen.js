'use strict';

var React = require('react-native');
var {
  View,
  StyleSheet,
  Text,
  TouchableHighlight,
} = React;

var InstructionsScreen = React.createClass({
  _onContinueButtonPress: function() {
    this.props.navigator.push({id: 3});
  },
  render: function() {
    return (
      <View style={styles.container}>
        <View style={styles.topSection}>
          <Text style={styles.instructionsText}>
            Focus on a number within 1 and 25. Don't forget it.
          </Text>
        </View>

        <View style={styles.bottomSection}>
          <TouchableHighlight
            style={styles.continueButton}
            onPress={this._onContinueButtonPress}>
            <Text
              style={styles.continueButtonText}>
              OK, I am ready
            </Text>
          </TouchableHighlight>
        </View>
      </View>
    )
  }
})

var styles = StyleSheet.create({
  container: {
    flex: 3,
    backgroundColor: '#F5FCFF',
  },
  topSection: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20,
  },
  instructionsText: {
    textAlign: 'center',
    color: "#223843",
    lineHeight: 40,
    fontSize: 25
  },
  bottomSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
    margin: 20
  },
  continueButton: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#223843",
    borderStyle: 'solid',
    borderColor: "#EFF1F3",
    borderWidth: 1,
    flex: 1,
    height: 60,
  },
  continueButtonText: {
    color: "#EFF1F3",
    fontSize: 20
  }
});

module.exports = InstructionsScreen;
