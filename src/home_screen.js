var React = require('react-native');
var {
  View,
  StyleSheet,
  Text,
  TouchableHighlight,
  Image
} = React;

var HomeScreen = React.createClass({
  startGame: function() {
    this.props.navigator.push({id: 2});
  },
  render: function() {
    return (
      <View style={styles.container}>
        <View style={styles.appHeader}>
          <Text style={styles.appTitle}>
            BitFlip
          </Text>
        </View>

        <View style={styles.instructionsBottomContainer}>
          <TouchableHighlight
            onPress={this.startGame}
            style={styles.playButton}>
            <Text style={styles.instructions}>
            Play
            </Text>
          </TouchableHighlight>
        </View>
      </View>
    )
  }
});

var styles = StyleSheet.create({
  container: {
    flex: 3,
    backgroundColor: '#F5FCFF',
  },
  appHeader: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    paddingTop: 120
  },
  appTitle: {
    fontSize: 40,
  },
  imageContainer: {
    flex: 1
  },
  instructionsBottomContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
    margin: 20
  },
  playButton: {
    borderColor: "#EFF1F3",
    backgroundColor: "#223843",
    justifyContent: 'center',
    alignItems: 'center',
    borderStyle: 'solid',
    borderWidth: 1,
    flex: 1,
    height: 60,
  },
  instructions: {
    fontSize: 25,
    color: "#EFF1F3",
  }
});

module.exports = HomeScreen;
